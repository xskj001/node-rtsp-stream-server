# node-rtsp-stream-server

#### 介绍
使用ffpemg推流的方式在H5中播放视频通道中的画面

#### 软件架构
软件架构说明


#### 安装教程

1.  git clone https://gitee.com/xskj001/node-rtsp-stream-server.git
2.  npm i
3.  npm run start

#### 使用说明

1.  服务端依赖 node-rtsp-stream / ffmpeg，下载后配置好Path （链接自己百度）
    https://github.com/kyriesent/node-rtsp-stream
2.  客户端依赖 jsmpeg 
    https://github.com/phoboslab/jsmpeg

   客户端演示，参照项目中的 index.html
  