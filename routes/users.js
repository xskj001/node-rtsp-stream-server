var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.setHeader("Content-Type", "text/html;charset=utf-8")
  res.send('<h1>Hello NOdejs</h1');
});

module.exports = router;
